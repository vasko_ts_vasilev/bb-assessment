var expanderToggler = document.getElementsByClassName("expander__toggle");
var i;

for (i = 0; i < expanderToggler.length; i++) {

    expanderToggler[i].addEventListener("click", function () {

        var panel = this.nextElementSibling;
        panel.classList.toggle("active");

    });
}